package pl.sda.builder.zad1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<GameCharacter> gameCharacterList = new ArrayList<GameCharacter>();

        GameCharacter gameCharacter=new GameCharacter.Builder().setHealth(150).setName("SuperHero").createGameCharacter();
        GameCharacter gameCharacter2=new GameCharacter.Builder().setHealth(140).setName("Leśna Nimfa").setMana(323).createGameCharacter();
        GameCharacter gameCharacter3=new GameCharacter.Builder().setHealth(155).setName("Człowiek Widmo").setNumberOfPoints(777).createGameCharacter();
        GameCharacter gameCharacter4=new GameCharacter.Builder().setHealth(158).setName("Głos ze zlewu").setMana(1000).createGameCharacter();


        gameCharacterList.add(gameCharacter);
        gameCharacterList.add(gameCharacter);
        gameCharacterList.add(gameCharacter2);
        gameCharacterList.add(gameCharacter3);
        gameCharacterList.add(gameCharacter4);
        gameCharacterList.add(gameCharacter);

        System.out.println(gameCharacterList.toString());


    }
}
