package pl.sda.builder.zad2;

public class Main {
    public static void main(String[] args) {

        Stamp stamp = new Stamp();
        System.out.println(stamp);

        Stamp stamp2 = new Stamp.Builder().setFirstDayNumber(0).setSecondDayNumber(4).setFirstMonthNumber(1).setSecondMonthNumber(2)
                .setYearNumber1(2).setYearNumber2(0).setYearNumber3(1).setYearNumber4(8).setCaseNumber(444).createStamp();
        System.out.println(stamp2);

    }
}
